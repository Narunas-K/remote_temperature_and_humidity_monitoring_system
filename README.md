Repository consists of:
- Code for monitoring node which measures temperature and humidity and sends measurements over WiFi to the server.
- Server side code to to store and to visualize retrieved data.

The project is based on teh hardware below:
--Node:
	-nodeMCU microcontrolled with integrated ESP8266 WiFi module. Programmed with Lua language

--Server side:
	-Raspberry PI 2 Model B microcomputer running Raspbian.