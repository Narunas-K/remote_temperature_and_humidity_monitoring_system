--INICIALIZACIJA IR PIN'Ų KONFIGURACIJA PRADŽIA { 
led=4 -- GPIO4
ledPower=3
dht22Pin=5 -- GPIO5
gpio.mode(led, gpio.OUTPUT)
--INICIALIZACIJA IR PIN'Ų KONFIGURACIJA PABAIGA }

-- KINTAMŲJŲ INICIALIZACIJA PRADŽIA {
value=gpio.LOW
counter=0
clockTicks=40
tempArray = {}
humArray = {}
tempMem={}
humMem={}
tempMem[0]=20
tempMem[1]=20
humMem[0]=45
humMem[1]=45
longTemp=0
longHum=0
tempSend=0
humSend=0
diffTemp=0
diffHum=0
nodeId='04'
i=0
previ=1
connected=false
wifiState=nil
--KINTAMŲJŲ INICIALIZACIJA PABAIGA}

-- WIFI INICIALIZACIJA PRADŽIA {

SSID="NAMAI"-- WIFI SSID
PSWD="kJggkJm0J!giJa0" --WIFI PSWD

--prisijungti prie WIFI ir išsaugoti konfigūraciją atmintyje [12] - čia nuoroda į šaltinį
wifi.setmode(wifi.STATION)
wifi.setphymode(wifi.PHYMODE_N)
station_cfg={}
station_cfg.ssid=SSID
station_cfg.pwd=PSWD
station_cfg.save=true
station_cfg.auto=true
wifi.sta.config(station_cfg)
wifi.setmode(wifi.NULLMODE)
--  WIFI INICIALIZACIJA PABAIGA}

--FUNKCIJŲ PRADŽIA{

-- Funkcija prisijungimui prie WiFI [13]
function wait_for_wifi_conn ( )
      if wifi.sta.getip ( ) == nil then
         print ("Waiting for Wifi connection")
         tmr.start(1)
      else
         tmr.stop (1)
         --ws = websocket.createClient()
         print ("ESP8266 mode is: " .. wifi.getmode ( ))
         print ("The module MAC address is: " .. wifi.ap.getmac ( ))
         print ("Config done, IP is " .. wifi.sta.getip ( ))
      end
end

-- Funkcija kuri sukuria žiniatinklio jungties (angl. web socket )klientą, prisijungia prie serverio ir įvykus žiniatinklio serverio įvykiams vykdo funkcijas
    -- [14]
function webSocketConnect()
    ws = websocket.createClient()
    ws:connect('ws://192.168.1.5:8888')
    ws:on("connection", function(ws)
        print('got ws connection')
        connected=true
    end)
    ws:on("receive", function(_, msg, opcode)
        print('got message:', msg, opcode) -- oppcode =1 tai tekstinė žinutė, o opcode=2 dvejetainis pranešimas
    end)
    ws:on("close", function(_, status)
        connected=false
        print('connection closed', status)
        ws = nil -- pagal reikalavimus būtina panaikinti žiniatinklio jungties klientą atsijungus
    end)
end

-- Funkcija žinutės persiuntimui į serverį
function toggleLED ()
    if wifi.sta.getip ( ) == nil then
        connected=false
    end
    if connected==true then
       value = gpio.LOW 
        print('esu siuntime')
        print(connected)
        if ((tonumber(longTemp) or tonumber(longHum)) >-60) then
        print(nodeId..longTemp..longHum)
        gpio.write(led, value)
        ws:send(nodeId..'T'..longTemp..'H'..longHum..'E')
        --čia T reiškia temperatūros duomenų pradžia, H - drėgmės duomenų pradžia, E - duomenų pabaiga
        end
    else
        webSocketConnect()
        value=gpio.HIGH
        gpio.write(led, value)
       print('esu siuntime bet ne cia')
        print(connected)
    end
    
end
--[15] funkcija skirta temperatūros nuskaitymui
function ReadTemp()
status, temp, humi, temp_dec, humi_dec = dht.read(dht22Pin)
    if status == dht.OK and counter<5  then
        tempArray[counter]=temp
        humArray[counter]=humi
        counter=counter+1
        tmr.start(0)
    print("DHT Temperature:"..temp..";".."Humidity:"..humi)

    elseif status == dht.ERROR_CHECKSUM then
        print( "DHT Checksum error." )
        tempArray[0]=-3000
        tempArray[1]=-3000
        tempArray[2]=-3000
        tempArray[3]=-3000
        tempArray[4]=-3000
        humArray[0]=-2000
        humArray[1]=-2000
        humArray[2]=-2000
        humArray[3]=-2000
        humArray[4]=-2000
        humArray[0]=-2000
   elseif status == dht.ERROR_TIMEOUT then
        print( "DHT timed out." )
        tempArray[0]=-3000
        tempArray[1]=-3000
        tempArray[2]=-3000
        tempArray[3]=-3000
        tempArray[4]=-3000
        humArray[0]=-2000
        humArray[1]=-200
        humArray[2]=-2000
        humArray[3]=-2000
        humArray[4]=-2000
        humArray[0]=-2000
    else 
    tmr.stop(0)
    end
   
end

-- funkcija virtualiems programos taktiniams impulsams formuoti

function clock ()
    if clockTicks>30 then
        gpio.write(ledPower, gpio.HIGH)
        clockTicks=0
        counter=0
        tempArray={0,0,0,0,0}
        humArray={0,0,0,0,0}
        tmr.start(0) -- paleidžiamas laikmatis skaitiklis temperatūros skaitymui, kad ji būtų nuskaityta penkis kartus iš eilės kas dvi sekundes
    elseif clockTicks==10 then
        wifi.setmode(wifi.STATION)
        wifi.sta.config(SSID,PSWD)
       
        if wifi.sta.getip ( ) ~= nil then
            connected=true
        end 
        tmr.start(1)
    elseif clockTicks==12 then
        tmr.stop(0) -- sustabdom laikmatį skaitiklį nr 0, kuris sužadina duomenų nuskaitymą iš jutiklio
        -- prisijungiam prie WiFi
        wifi.setmode(wifi.STATION)
        wifi.sta.config(SSID,PSWD)
        tmr.start(1)
        wait_for_wifi_conn ( )
        -- Čia temperatūros ir drėgmės penkių matavimų duomenis suvidurkinam ir įrašom į siuntimo kintamuosius
        tempSend=(tempArray[0]+tempArray[1]+tempArray[2]+tempArray[3]+tempArray[4])/5
        humSend=(humArray[0]+humArray[1]+humArray[2]+humArray[3]+humArray[4])/5
        longTemp=tostring(tempSend)
        longHum=tostring(humSend)
        print("TEMP "..longTemp)
        print("HUM "..longHum)
        print(longHum)
        print(longTemp)  
    elseif clockTicks==21 then --dar kartą paleidžiam siuntimo funkcijos laikmatį skaitiklį
        toggleLED ()
    elseif clockTicks==23 then 
        toggleLED ()
    elseif clockTicks==25 then -- dar kartą paleidžiam siuntimo funkcijos laikmatį skaitiklį ir išjungiam Wifi
        tmr.stop(1)  
        value=gpio.HIGH
        gpio.write(led, value)
        wifi.setmode(wifi.NULLMODE)
        connected=false
        print('KONFIGURUOJAMMMMMMMMMMMMMMMM')
    elseif clockTicks==26 then
        gpio.write(ledPower, gpio.LOW)
        node.dsleep(300000000)
        --node.dsleep(5000000)
    end
    print(clockTicks)
    clockTicks=clockTicks+1
end
-- FUNKCIJŲ PABAIGA }

--LAIKRODŽIŲ SKAITIKLIŲ INICIALIZAVIMAS PRADŽIA {
tmr.alarm (2, 500, tmr.ALARM_SINGLE, toggleLED )
tmr.alarm (1, 1000, tmr.ALARM_SINGLE, wait_for_wifi_conn)
tmr.alarm(0, 2000, tmr.ALARM_SEMI , ReadTemp)
tmr.alarm (3, 1000, tmr.ALARM_AUTO, clock )
-- LAIKRODŽIŲ SKAITIKLIŲ INICIALIZAVIMAS PABAIGA }
