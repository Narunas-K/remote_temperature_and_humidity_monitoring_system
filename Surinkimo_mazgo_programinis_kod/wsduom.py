#!/usr/bin/python
# -*- coding: utf-8 -*-
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import time
import sqlite3 as lite
import sys

class WSHandler(tornado.websocket.WebSocketHandler): # [16] šaltinis
  def check_origin(self, origin):
    return True
  # Vykdoma, kai iškviečiamas įvykis ,,Mazgas prisijungė''
  def open(self):
    print 'Mazgas prisijunge.\n'
    now=time.strftime("%Y-%m-%d")
    laikas=time.strftime("%H:%M:%S")
    print now
    print laikas

  # Vykdoma, kai išskviečiamas įvykis ,,Mazgas prisijungė''
  def on_message(self, message):
    data=time.strftime("%Y-%m-%d") # nuskaitomas žinutes gavimo laikas
    laikas=time.strftime("%H:%M:%S")
    received_m=message
    nodeid=received_m[0]+received_m[1] # iš žinutės išskiriamas mazgo numeris
    index_T=received_m.index('T') # Simbolis 'T' žymi temperatūros duomenis žinuteje
    index_H=received_m.index('H') # Simbolis 'H' žymi drėgmės duomenis žinuteje
    index_E=received_m.index('E') # Simbolis 'E' žymi žinutes pabaiga
    length_m=len(received_m)

    #Iš žinutes atskiriama temperatūra
    pointer_T=index_T+1
    raw_temp=received_m[pointer_T]
    pointer_T=pointer_T+1
    while(pointer_T<index_H):
      raw_temp=raw_temp+received_m[pointer_T]
      pointer_T=pointer_T+1
    print(raw_temp)

    #Iš žinutes atskiriama drėgmės informacija
    pointer_H=index_H+1
    raw_hum=received_m[pointer_H]
    pointer_H=pointer_H+1
    while(pointer_H<index_E):
      raw_hum=raw_hum+received_m[pointer_H]
      pointer_H=pointer_H+1
    print(raw_hum)

    temp=raw_temp # išskirta temperatūros reikšmė iš žinutes priskiriama kintamajam temp
    hum=raw_hum # išskirta drėgemės reikšmė iš žinutės priskiriama kintamajam hum

    con = lite.connect("templog.db") #prijungiama duomenų bazė. [17] šaltinis
    with con:
      cur = con.cursor()
      cur.execute("INSERT INTO duomenuLentele1 VALUES (?, ?, ?, ?, ? )", (nodeid,data, laikas, temp, hum)) #įrašomi duomenys į duomenų bazę
    print data
    print laikas
    print 'Priimta zinute: %s\n' %message
    self.write_message(message + ' OK') # išsiunciama priimta žinutė + OK sensoriniam mazgui kaip priėmimo patvirtinimas

    #Vykdoma, kai išskvieciamas įvykis ,,Mazgas atsijungė''
  def on_close(self):
    print 'Mazgas atsijunge\n'

application = tornado.web.Application([(r'/', WSHandler),])

if __name__ == "__main__":
  http_server = tornado.httpserver.HTTPServer(application)
  http_server.listen(8888)
  tornado.ioloop.IOLoop.instance().start()
