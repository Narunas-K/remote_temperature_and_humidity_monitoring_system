<!DOCTYPE html>
<html>
<head>
<style>
table {
    width: 100%;
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
    padding: 5px;
}

th {text-align: left;}
</style>
</head>
<body>

<form action="getnode.php">
<select name="q" >
  <option value="">Pasirinkite mazgą:</option>
  <option value="1">Mazgas NR.1</option>
  <option value="2">Mazgas NR.2</option>
  <option value="3">Mazgas NR.3</option>
  <option value="4">Mazgas NR.4</option>
  </select>

<select name="data">
  <option value="">Pasirinkite metus:</option>
  <option value="2020">2020 metai</option>
  <option value="2019">2019 metai</option>
  <option value="2018">2018 metai</option>
  <option value="2017">2017 metai</option>
  <option value="2016">2016 metai</option>
  <option value="2015">2015 metai</option>
  <option value="2014">2014 metai</option>
  </select>

<select name="menuo">
  <option value="">Pasirinkite mėnesį:</option>
  <option value="12">Gruodis</option>
  <option value="11">Lapkritis</option>
  <option value="10">Spalis</option>
  <option value="9">Rugsėjis</option>
  <option value="8">Rugpjūtis</option>
  <option value="7">Liepa</option>
  <option value="6">Birzželis</option>
  <option value="5">Gegužė</option>
  <option value="4">Balandis</option>
  <option value="3">Kovas</option>
  <option value="2">Vasaris</option>
  <option value="1">Sausis</option>
  </select>

<select name="diena">
  <option value="">Pasirinkite dieną:</option>
  <option value="31">31 diena</option>
  <option value="30">30 diena</option>
  <option value="29">29 diena</option>
  <option value="28">28 diena</option>
  <option value="27">27 diena</option>
  <option value="26">26 diena</option>
  <option value="25">25 diena</option>
  <option value="24">24 diena</option>
  <option value="23">23 diena</option>
  <option value="22">22 diena</option>
  <option value="21">21 diena</option>
  <option value="20">20 diena</option>
  <option value="19">19 diena</option>
  <option value="18">18 diena</option>
  <option value="17">17 diena</option>
  <option value="16">16 diena</option>
  <option value="15">15 diena</option>
  <option value="14">14 diena</option>
  <option value="13">13 diena</option>
  <option value="12">12 diena</option>
  <option value="11">11 diena</option>
  <option value="10">10 diena</option>
  <option value="9">9 diena</option>
  <option value="8">8 diena</option>
  <option value="7">7 diena</option>
  <option value="6">6 diena</option>
  <option value="5">5 diena</option>
  <option value="4">4 diena</option>
  <option value="3">3 diena</option>
  <option value="2">2 diena</option>
  <option value="1">1 diena</option>
  </select>

<select name="valanda" >
  <option value="">Pasirinkite valandą:</option>
  <option value="101">00 valanda</option>
  <option value="23">23 valanda</option>
  <option value="22">22 valanda</option>
  <option value="21">21 valanda</option>
  <option value="20">20 valanda</option>
  <option value="19">19 valanda</option>
  <option value="18">18 valanda</option>
  <option value="17">17 valanda</option>
  <option value="16">16 valanda</option>
  <option value="15">15 valanda</option>
  <option value="14">14 valanda</option>
  <option value="13">13 valanda</option>
  <option value="12">12 valanda</option>
  <option value="11">11 valanda</option>
  <option value="10">10 valanda</option>
  <option value="9">9 valanda</option>
  <option value="8">8 valanda</option>
  <option value="7">7 valanda</option>
  <option value="6">6 valanda</option>
  <option value="5">5 valanda</option>
  <option value="4">4 valanda</option>
  <option value="3">3 valanda</option>
  <option value="2">2 valanda</option>
  <option value="1">1 valanda</option>
  </select>
<input type="submit">
</form>

<?php

$q=intval($_GET['q']);
$data=intval($_GET['data']);
$menuo=intval($_GET['menuo']);
$diena=intval($_GET['diena']);
$valanda=intval($_GET['valanda']);

if($menuo==1){
	$menuo='01';
}
else if($menuo==2){
	$menuo='02';
}
else if($menuo==3){
	$menuo='03';
}
else if($menuo==4){
	$menuo='04';
}
else if($menuo==5){
	$menuo='05';
}
else if($menuo==6){
	$menuo='06';
}
else if($menuo==7){
	$menuo='07';
}
else if($menuo==8){
	$menuo='08';
}
else if($menuo==9){
	$menuo='09';
}


if($diena==1){
	$diena='01';
}
else if($diena==2){
	$diena='02';
}
else if($diena==3){
	$diena='03';
}
else if($diena==4){
	$diena='04';
}
else if($diena==5){
	$diena='05';
}
else if($diena==6){
	$diena='06';
}
else if($diena==7){
	$diena='07';
}
else if($diena==8){
	$diena='08';
}
else if($diena==9){
	$diena='09';
}

// datos kintamojo sukūrimas, kuriuo bus kreipiamas į duomenų bazę
$dataPilna=$data . '-' . $menuo . '-' . $diena;

class MyDB extends SQLite3
   {
      function __construct()
      {
         $this->open('/var/www/templog.db');
      }
   }
   $db = new MyDB();
   if(!$db){
      echo $db->lastErrorMsg();
   } else {
      echo "<br>";
   }

// 1. išrenkami duomenys pagal mazgo numerį
if($data==0 AND $menuo==0 AND $diena==0 AND $valanda==0){
echo 'as pirmam';
$sql =<<<EOF
	SELECT * from duomenuLentele1 WHERE nodeid=$q;
EOF;
$ret = $db->query($sql);
}
// 2. išrenkami duomenys pagal mazgo numerį ir datą
else if ($valanda==0){
echo 'as antram';
$sql =<<<EOF
	SELECT * from duomenuLentele1 WHERE nodeid=$q AND data='$dataPilna';
EOF;
$ret = $db->query($sql);
}
//3. išrenkami duomenys pagal mazgą, datą ir laiką
else if ($q!=0 AND $data!=0 AND $menuo!=0 AND $diena!=0 AND $valanda!=0){
	echo 'labas as treciam';
	if($valanda==101){
		$valanda='00';
	}

$sql =<<<EOF
	SELECT * from duomenuLentele1 WHERE nodeid=$q AND data='$dataPilna' AND strftime('%H',laikas)='$valanda';
EOF;
$ret = $db->query($sql);
}
// 4. išrenkami duomenys pagal mazgo numeri ir valandą
else if ($q!=0 AND $data==0 AND $menuo==0 AND $diena==0 AND $valanda!=0){
echo 'as ketvirtam';
$sql =<<<EOF
	SELECT * from duomenuLentele1 WHERE nodeid=$q AND strftime('%H',laikas)='$valanda';
EOF;
$ret = $db->query($sql);
}
// 5. išrenkami duomenys pagal valandą
else if ($q==0 AND $data==0 AND $menuo==0 AND $diena==0 AND $valanda!=0){
echo 'as penktam';
if($valanda==101){
		$valanda='00';
	}
$sql =<<<EOF
	SELECT * from duomenuLentele1 WHERE strftime('%H',laikas)='$valanda';
EOF;
$ret = $db->query($sql);
}


// [18] šaltinis
echo "<table>
<tr>
<th>Mazgo nr</th>
<th>Data</th>
<th>Laikas</th>
<th>Temperatura C</th>
<th>Dregme %</th>
</tr>";
while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
    echo "<tr>";
    echo "<td>" . $row['nodeid'] . "</td>";
    echo "<td>" . $row['data'] . "</td>";
    echo "<td>" . $row['laikas'] . "</td>";
    echo "<td>" . $row['temp'] . "</td>";
    echo "<td>" . $row['hum'] . "</td>";
    echo "</tr>";
}
echo "</table>";
$db->close();
?>
</body>
</html>
